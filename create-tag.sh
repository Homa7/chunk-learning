#!/bin/bash
if  [ "$1" = "" ] ; then
    echo "Please supply a new version."
    exit
fi

upstream_repo=git@gitlab.com:Homa7/chunk-learning.git
git checkout master
#git pull $upstream_repo master
#git reset --hard upstream/master
#npm ci
npm version --no-git-tag-version $1
#npm run build:prod
if [ $? -eq 0 ]
then
  echo "Build succeeded"

  # publish
  PACKAGE_VERSION=$(cat package.json |
    grep version |
    head -1 |
    awk -F: '{ print $2 }' |
    sed 's/[",]//g' |
    tr -d '[[:space:]]')
  git add package.json package-lock.json
  git commit -m "Publish version $1"
  git tag $1
  git push $upstream_repo master --no-verify
  git push $upstream_repo --no-verify $1
  echo "Pushed tag successfuly"

else
    echo "failed"
fi
